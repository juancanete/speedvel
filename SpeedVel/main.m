//
//  main.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 02/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
