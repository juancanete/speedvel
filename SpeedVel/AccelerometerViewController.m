//
//  AccelerometerViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 30/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "AccelerometerViewController.h"

@interface AccelerometerViewController ()

@end

@implementation AccelerometerViewController

@synthesize imgPoint;

#pragma mark - Methods

- (IBAction) onClickInitPosition
{
    motionMButton = [[CMMotionManager alloc] init];
    
    if ([motionMButton isAccelerometerAvailable] == YES) {
        [motionMButton startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error){
            xMainRelative = accelerometerData.acceleration.x + accelerometerData.acceleration.z;
            yMainRelative = accelerometerData.acceleration.y;
            
            [motionMButton stopAccelerometerUpdates];
        }];
        
        
    }
}

#pragma mark - View Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    xMainRelative = 0;
    yMainRelative = 0;
    
    motionManager = [[CMMotionManager alloc] init];
    
    //Set configuration accelerometer
    if ([motionManager isAccelerometerAvailable] == YES) {
        [motionManager setAccelerometerUpdateInterval:1/kFrecuencyAccelerometer];
        [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error){
            int x = ((accelerometerData.acceleration.x + accelerometerData.acceleration.z) - xMainRelative) * 9.81;
            int y = (accelerometerData.acceleration.y - yMainRelative) * 2 * 9.81;
            
            //NSLog(@"XRelative: %f, YRelative: %f", xMainRelative, yMainRelative);
            [imgPoint setCenter:CGPointMake(KCenterImagePoint + y, KCenterImagePoint + x)];
        }];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    [motionManager stopAccelerometerUpdates];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
