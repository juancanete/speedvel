//
//  ImageCellView.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 15/08/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "ImageCellView.h"

@implementation ImageCellView

@synthesize imgBackSpeedo;
@synthesize imgShadow;

#pragma mark - Methods

- (void) setImageBack
{
    //Set image to Setting screen
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [imgBackSpeedo setImage:appDelegate.imgBackgroundLand.image];
}

- (void) setFeaturesImageView
{
    //Set shadow and border to UIImageView
    
    //imgBackSpeedo
    //Set Corner Radius 
    [imgBackSpeedo.layer setCornerRadius:3.0];
    imgBackSpeedo.layer.masksToBounds = YES;
    
    //Border
    imgBackSpeedo.layer.borderColor = [UIColor grayColor].CGColor;
    imgBackSpeedo.layer.borderWidth = 1;
    
    //ImgShadow
    imgShadow.layer.shadowColor = [UIColor blackColor].CGColor;
    imgShadow.layer.shadowOffset = CGSizeMake(0, 1);
    imgShadow.layer.shadowOpacity = 0.8;
    imgShadow.layer.shadowRadius = 3.0;
    
    
    
    //imgBackSpeedo.layer.opaque = NO;
}
#pragma mark - View Life cycle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
