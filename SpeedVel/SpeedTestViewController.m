//
//  SpeedTestViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 06/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "SpeedTestViewController.h"

@interface SpeedTestViewController ()

@end

@implementation SpeedTestViewController

@synthesize nvTimer;
@synthesize vwContainer;
@synthesize btnStartPause;
@synthesize btnStop;
@synthesize lblAverageSp;
@synthesize lblSpeed;
@synthesize lblAveMearure;
@synthesize lblSpeedMeasure;

//Time Label
@synthesize lblMilliSeconds;
@synthesize lblMinutes;
@synthesize lblSeconds;
@synthesize lblTwoPoint1;
@synthesize lblTwoPoint2;

#pragma mark - AdBannerDelegate
- (void) bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"show banner");
    [self adjustBannerView];
}

- (void) bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Error!!!!: %@", [error description]);
    [self adjustBannerView];
}

#pragma mark - CLLocationManagerDelegate

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [arrSpeeds addObject:manager.location];
    
    [lblSpeed setText:[NSString stringWithFormat:@"%.1f",manager.location.speed]];
    [self figureOutAverageSpeed:arrSpeeds];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //[lblError setText:[error description]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Speed Error" message:@"No hay señal" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - iAdBanner Methods

- (void) createAdBannerView
{
    bannerView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    CGRect bannerFrame = bannerView.frame;
    bannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.height;//self.view.frame.size.height;
    bannerView.frame = bannerFrame;
    
    //Delegate
    [bannerView setDelegate:self];
}

- (void) adjustBannerView
{
    CGRect contentView = self.vwContainer.frame;
    CGRect adBannerFrame = bannerView.frame;
    
    if ([bannerView isBannerLoaded]) {
        contentView.origin.y = kOriginalOrigen - kHeightBanner;
        
        if ([appDelegate isiOS7]) {
            adBannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.height - kHeightTabBar - kHeightBanner;
        }else
        {
            adBannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.height - kHeightTabBar - kHeightBanner - kHeightStatusBar;
        }
        
    }else
    {
        contentView.origin.y = kOriginalOrigen;
        adBannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.height;
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        self.vwContainer.frame = contentView;
        bannerView.frame = adBannerFrame;
    }];
}

#pragma mark - Methods

- (void) figureOutAverageSpeed:(NSMutableArray *)locations
{
    float flSpeeds = 0.0;
    CLLocation *locSpeed;
    
    if ([locations count] > 9) {
        for (int i = 0; i < [locations count]; i++) {
            locSpeed = [locations objectAtIndex:i];
            flSpeeds += locSpeed.speed;
        }
        
        //Calculate Average Speed
        [lblAverageSp setText:[NSString stringWithFormat:@"%.1f",(flSpeeds/[locations count])]];
    }
}

- (void) setupLocationManager
{
    //Create Manager Object
    locManager = [[CLLocationManager alloc] init];
    [locManager setDelegate:self];
    [locManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    //Once configurated all setting, the location manager must "start location"
    [locManager startUpdatingLocation];
}

- (void) stopLocation
{
    //Stop acceleration proccess    
    [locManager stopUpdatingLocation];
}

- (void) chronometerLabel
{
    if (miliseconds < 99) {
        miliseconds++;
    }else if ((miliseconds == 99) && (seconds < 59))
    {
        miliseconds = 0;
        seconds++;
    }else if ((seconds == 59) && (minutes < 59))
    {
        seconds = 0;
        minutes++;
    }
    
    //Timer
    if ((minutes <= 9) && (seconds <= 9) && (miliseconds <= 9)) {
        [lblMinutes setText:[NSString stringWithFormat:@"0%i",minutes]];
        [lblSeconds setText:[NSString stringWithFormat:@"0%i",seconds]];
        [lblMilliSeconds setText:[NSString stringWithFormat:@"0%i",miliseconds]];
        //[lblTimer setText: [NSString stringWithFormat:@"0%i:0%i:0%i",minutes,seconds,miliseconds]];
    }else if ((seconds <= 9) && (miliseconds <= 9))
    {
        [lblMinutes setText:[NSString stringWithFormat:@"%i",minutes]];
        [lblSeconds setText:[NSString stringWithFormat:@"0%i",seconds]];
        [lblMilliSeconds setText:[NSString stringWithFormat:@"0%i",miliseconds]];
        //[lblTimer setText: [NSString stringWithFormat:@"%i:0%i:0%i",minutes,seconds,miliseconds]];
    }else if ((minutes <= 9) && (miliseconds <= 9))
    {
        [lblMinutes setText:[NSString stringWithFormat:@"0%i",minutes]];
        [lblSeconds setText:[NSString stringWithFormat:@"%i",seconds]];
        [lblMilliSeconds setText:[NSString stringWithFormat:@"0%i",miliseconds]];
        //[lblTimer setText: [NSString stringWithFormat:@"0%i:%i:0%i",minutes,seconds,miliseconds]];
    }else if ((minutes <= 9) && (seconds <= 9))
    {
        [lblMinutes setText:[NSString stringWithFormat:@"0%i",minutes]];
        [lblSeconds setText:[NSString stringWithFormat:@"0%i",seconds]];
        [lblMilliSeconds setText:[NSString stringWithFormat:@"%i",miliseconds]];
        //[lblTimer setText: [NSString stringWithFormat:@"0%i:0%i:%i",minutes,seconds,miliseconds]];
    }else if (minutes <= 9)
    {
        [lblMinutes setText:[NSString stringWithFormat:@"0%i",minutes]];
        [lblSeconds setText:[NSString stringWithFormat:@"%i",seconds]];
        [lblMilliSeconds setText:[NSString stringWithFormat:@"%i",miliseconds]];
        //[lblTimer setText: [NSString stringWithFormat:@"0%i:%i:%i",minutes,seconds,miliseconds]];
    }else if (seconds <= 9)
    {
        [lblMinutes setText:[NSString stringWithFormat:@"%i",minutes]];
        [lblSeconds setText:[NSString stringWithFormat:@"0%i",seconds]];
        [lblMilliSeconds setText:[NSString stringWithFormat:@"%i",miliseconds]];
        //[lblTimer setText: [NSString stringWithFormat:@"%i:0%i:%i",minutes,seconds,miliseconds]];
    }else if (miliseconds <= 9) //Error?
    {
        [lblMinutes setText:[NSString stringWithFormat:@"%i",minutes]];
        [lblSeconds setText:[NSString stringWithFormat:@"%i",seconds]];
        [lblMilliSeconds setText:[NSString stringWithFormat:@"0%i",miliseconds]];
        //[lblTimer setText: [NSString stringWithFormat:@"%i:%i:0%i",minutes,seconds,miliseconds]];
    }else
    {
        [lblMinutes setText:[NSString stringWithFormat:@"%i",minutes]];
        [lblSeconds setText:[NSString stringWithFormat:@"%i",seconds]];
        [lblMilliSeconds setText:[NSString stringWithFormat:@"%i",miliseconds]];
        //[lblTimer setText: [NSString stringWithFormat:@"%i:%i:%i",minutes,seconds,miliseconds]];
    }
}

#pragma mark - IBActions Methods

- (IBAction)onClickStartStop:(id)sender
{
    if (![sender isKindOfClass:[UIButton class]]) {
        return;
    }
    
    int tag = [(UIButton *) sender tag];
    
    switch (tag) {
        case 0:
                    
            timerChronometer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(chronometerLabel) userInfo:nil repeats:YES];
            [btnStartPause setTitle:@"Pause" forState:UIControlStateNormal];
            [btnStartPause setTag:1];
            [self setupLocationManager];
            
            break;
        case 1:
            
            [timerChronometer invalidate];
            [btnStartPause setTitle:@"Start" forState:UIControlStateNormal];
            [btnStartPause setTag:0];
            [lblSpeed setText:@"0.0"];
            [self stopLocation];
            
            break;
        case 2:
            
            //Check if start
            if ([btnStartPause.titleLabel.text isEqualToString:@"Pause"]) {
                [btnStartPause setTitle:@"Start" forState:UIControlStateNormal];
                [btnStartPause setTag:0];
            }
            
            //Init Variables
            miliseconds = 0;
            seconds = 0;
            minutes = 0;
            
            //Timer
            [lblMinutes setText:@"00"];
            [lblSeconds setText:@"00"];
            [lblMilliSeconds setText:@"00"];

            [lblSpeed setText:@"0.0"];
            [lblAverageSp setText:@"0.0"];
            [timerChronometer invalidate];
            [self stopLocation];
            
            break;
            
        default:
            break;
    }
}

#pragma mark - View Life Cycle

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated
{
    //Show Km/h or mph
    if ([appDelegate isKmhOrMph]) {
        //Miles
        [lblSpeedMeasure setText:@"mph"];
        [lblAveMearure setText:@"mph"];
    }else
    {
        //km/h
        [lblSpeedMeasure setText:@"km/h"];
        [lblAveMearure setText:@"km/h"];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Init appDelegate
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //Init chronometer variables
    miliseconds = 0;
    seconds = 0;
    minutes = 0;
    stTimer = [[NSMutableString alloc] init];
    arrSpeeds = [[NSMutableArray alloc] init];
    
    //Banner
    [self createAdBannerView];
    [self.view addSubview:bannerView];
    
    //Init view properties
    [nvTimer setTintColor:[UIColor colorWithRed:0.0627451 green:0.22745098 blue:0.67058824 alpha:1]];
    
    //UINavigationBar
    if (![appDelegate isiOS7]) {
        [nvTimer setFrame:CGRectMake(0, 0, nvTimer.frame.size.width, nvTimer.frame.size.height)];
        [vwContainer setFrame:CGRectMake(0, kOriginalOrigen, vwContainer.frame.size.width, vwContainer.frame.size.height)];
    }
    
    //Set labels features
    UIFont *font = [UIFont fontWithName:@"DS-Digital" size:45];
    [lblTwoPoint2 setFont:font];
    [lblTwoPoint1 setFont:font];
    [lblSeconds setFont:font];
    [lblMinutes setFont:font];
    [lblMilliSeconds setFont:font];
    [lblSpeed setFont:font];
    [lblAverageSp setFont:font];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
