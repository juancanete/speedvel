//
//  MeasureCellView.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 15/08/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "MeasureCellView.h"

@implementation MeasureCellView

@synthesize sgmMeasure;

#pragma mark - Methods

- (IBAction)onSelectMeasure
{
    //Check value AppDelegate
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"object 0: %@ object 1: %@",[appDelegate.arrContentFile objectAtIndex:0],[appDelegate.arrContentFile objectAtIndex:1]);
    
    //Init value NSNumber
    NSNumber *nbBool = [appDelegate.arrContentFile objectAtIndex:0];
    
    //Refresh values into appDelegate
    [appDelegate.arrContentFile removeAllObjects];
    
    //boolean
    BOOL bIsKmhOrMpH;
    
    if ([sgmMeasure selectedSegmentIndex] == 0) {
        //KMH
        bIsKmhOrMpH = NO;
    }else
    {
        //MPH
        bIsKmhOrMpH = YES;
    }
    
    appDelegate.arrContentFile = [[NSMutableArray alloc] initWithObjects:nbBool,[NSNumber numberWithBool:bIsKmhOrMpH], nil];
    
    NSLog(@"object 0: %@ object 1: %@",[appDelegate.arrContentFile objectAtIndex:0],[appDelegate.arrContentFile objectAtIndex:1]);
}

#pragma mark - View Life cycle

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
