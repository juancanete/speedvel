//
//  SecondViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 02/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "GPSCellView.h"
#import "MeasureCellView.h"
#import "ImageCellView.h"
#import "SWRevealViewController.h"

#define kHeightTableView [[UIScreen mainScreen] bounds].size.height < 568 ? 415:505

@interface ConfigViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    AppDelegate *appDelegate;
    UIImagePickerController *imgPickerPhoto;
}

@property (nonatomic,retain) IBOutlet UITableView *tblSettings;
@property (nonatomic,retain) IBOutlet UINavigationBar *nvSettings;
@property (nonatomic,retain) IBOutlet UITabBarItem *tbiSettings;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *sidebarButton;

- (IBAction)onClickSelectImage: (id) sender;
- (IBAction)onClickBack;
@end
