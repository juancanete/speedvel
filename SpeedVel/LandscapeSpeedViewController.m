//
//  LandscapeSpeedViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 19/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "LandscapeSpeedViewController.h"

@interface LandscapeSpeedViewController ()

@end

@implementation LandscapeSpeedViewController

@synthesize lblMeasure;
@synthesize lblSpeed;
@synthesize pgrThrottle;
@synthesize pgrBrake;
@synthesize imgLandscapeBack;
@synthesize vwOriginal;
@synthesize vwContainerView;
@synthesize imgAnalogSpeedo;
@synthesize imgSpeedometer;
@synthesize imgDigitalSpeedo;

#pragma mark - AdBannerDelegate
- (void) bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"show land banner");
    [self adjustBannerView];
}

- (void) bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Error!!!!: %@", [error description]);
    [self adjustBannerView];
}

- (BOOL) bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"AdBannerView has been pushed!");
    
    return YES;
}

#pragma mark - AdmobBannerDelegate

- (void) adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"ADMOB Receive an ad");
}

#pragma mark - iAdBanner Methods

- (void) createAdBannerView
{
    //Admob
    bannerAdmob = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, 40)];
    bannerAdmob.adUnitID = [NSString stringWithFormat:@"%@",idUniqueAds];
    
    bannerAdmob.rootViewController = self;
    [bannerAdmob setDelegate:self];
    [self.vwOriginal addSubview:bannerAdmob];
    
    [bannerAdmob loadRequest:[GADRequest request]];
}

- (void) adjustBannerView
{
    CGRect contentView = self.vwOriginal.frame;
    CGRect adBannerFrame = bannerLandView.frame;
    
    if ([bannerLandView isBannerLoaded]) {
        contentView.origin.y = kLandOriginalOrigin - kHeightBannerAdmob;
        adBannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.width - kHeightBannerAdmob;
    }else
    {
        contentView.origin.y = kLandOriginalOrigin;
        adBannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.width;// self.view.frame.size.height;
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        self.vwOriginal.frame = contentView;
        bannerLandView.frame = adBannerFrame;
    }];
}

#pragma mark - Methods View

- (void) updateElementsView
{
    if (appDelegate.flSpeed >= 0.0) {
        [lblSpeed setText:appDelegate.stSpeed];
        [pgrThrottle setProgress:appDelegate.flThrottle];
        [pgrBrake setProgress:appDelegate.flBrake];
        [self rotateImageNeedle:appDelegate.flSpeed];
    }
}

- (void) initAllElements
{
    [self rotateImageNeedle:0];
    [lblSpeed setText:@"0.0"];
    [pgrBrake setProgress:0.0 animated:YES];
    [pgrThrottle setProgress:0.0 animated:YES];
}

#pragma mark - Private Methods

float Degrees2Radians(float degrees)
{
    return degrees * (M_PI / 320);
}

- (void) rotateImageNeedle: (float) speed
{
    //if (speed <= 120) {
      //  speed += 1;
        imgNeedle.transform = CATransform3DMakeRotation(Degrees2Radians(speed), 0, 0, 1);
        
    //}else
    //{
        //[timerSpeed invalidate];
    //}
    
    
}

#pragma mark - View life cycle

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    NSLog(@"it should be rotate!!");
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //AppDelegate
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    //Init view values
    [self initAllElements];
    
    //Speed Label
    UIFont *font = [UIFont fontWithName:@"DS-Digital" size:40];
    [lblSpeed setFont:font];
    //[lblSpeed setShadowColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1]];
    //[lblSpeed setFont:[UIFont fontWithName:@"Helvetica" size:24]];
    //[lblSpeed setTextColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:1]];
    
    //Set point rotating image
    //imgNeedle.center = CGPointMake(210, 190);
    imgNeedle = [CALayer layer];
    imgNeedle.contents = (__bridge id)([UIImage imageNamed:@"velocimetro-aguja2.png"].CGImage);
    
    //Set frame Image Needle, UIProgressView and ContainerView
    if ([[UIScreen mainScreen] bounds].size.height <= 480) {
        //Needle
        imgNeedle.frame = CGRectMake(220, 218, 60, 20);
        imgNeedle.bounds = CGRectMake(0, 0, CGImageGetWidth((__bridge CGImageRef)imgNeedle.contents) + 15, CGImageGetHeight((__bridge CGImageRef)imgNeedle.contents));
        
        //UIProgressView
        [pgrThrottle setFrame:CGRectMake(pgrThrottle.frame.origin.x, pgrThrottle.frame.origin.y, pgrThrottle.frame.size.width - kDifferenceResolution, pgrThrottle.frame.size.height)];
        [pgrBrake setFrame:CGRectMake(pgrBrake.frame.origin.x, pgrBrake.frame.origin.y, pgrBrake.frame.size.width - kDifferenceResolution, pgrBrake.frame.size.height)];
        
        //Container View
        [vwContainerView setFrame:CGRectMake(vwContainerView.frame.origin.x - kDifferenceResolution, vwContainerView.frame.origin.y, vwContainerView.frame.size.width, vwContainerView.frame.size.height)];
    }else
    {
        imgNeedle.frame = CGRectMake(260, 218, 60, 20);
        imgNeedle.bounds = CGRectMake(0, 0, CGImageGetWidth((__bridge CGImageRef)imgNeedle.contents) + 45, CGImageGetHeight((__bridge CGImageRef)imgNeedle.contents));
    }
    
    imgNeedle.anchorPoint = CGPointMake(1, 1);
    
    //Set features objects
    //Landscape back
    
    //Set size
    NSLog(@"resize: %i",kResizeImages);
    
    [imgLandscapeBack setFrame:CGRectMake(imgLandscapeBack.frame.origin.x, imgLandscapeBack.frame.origin.y, imgLandscapeBack.frame.size.width - kResizeImages, imgLandscapeBack.frame.size.height)];
    [imgSpeedometer setFrame:CGRectMake(imgSpeedometer.frame.origin.x, imgSpeedometer.frame.origin.y, imgSpeedometer.frame.size.width - kResizeImages, imgSpeedometer.frame.size.height)];
    [imgAnalogSpeedo setFrame:CGRectMake(imgAnalogSpeedo.frame.origin.x, imgAnalogSpeedo.frame.origin.y, imgAnalogSpeedo.frame.size.width - kResizeImages, imgAnalogSpeedo.frame.size.height)];
    
    //Set center
    [imgLandscapeBack setCenter:CGPointMake(kCenterImages, imgLandscapeBack.center.y)];
    [imgSpeedometer setCenter:CGPointMake(kCenterImages, imgSpeedometer.center.y)];
    [imgAnalogSpeedo setCenter:CGPointMake(kCenterImages, imgAnalogSpeedo.center.y)];
    [imgDigitalSpeedo setCenter:CGPointMake(kCenterImages, imgDigitalSpeedo.center.y)];
    
    
    //Labels
    [lblMeasure setCenter:CGPointMake(kCenterImages, lblMeasure.center.y)];
    [lblSpeed setCenter:CGPointMake(kCenterImages, lblSpeed.center.y)];
    
    //Insert layer to Superview
    [self.vwOriginal.layer addSublayer:imgNeedle];
    
    //Size UIProgressView
    if ([appDelegate isiOS7]) {
        pgrThrottle.transform = CGAffineTransformMakeScale(1, 4);
        pgrBrake.transform = CGAffineTransformMakeScale(1, 4);
    }
    
    //Set image landscape
    [imgLandscapeBack setImage:appDelegate.imgBackgroundLand.image];
    
    //AdBanner
    [self createAdBannerView];
}

- (void) viewDidAppear:(BOOL)animated
{
    //Init timer
    if (appDelegate.bIsGPSActivated) {
        timerValues = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateElementsView) userInfo:nil repeats:YES];
    }
    
    //Hide Status Bar
    if ([appDelegate isiOS7]) {
        [self prefersStatusBarHidden];
    }else
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    
    //Set measure
    if ([appDelegate isKmhOrMph]) {
        //MPH
        [lblMeasure setText:@"mph"];
    }else
    {
        //Km/h
        [lblMeasure setText:@"Km/h"];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    //Stop timer
    [timerValues invalidate];
}

- (void) viewWillDisappear:(BOOL)animated
{
    //Show Status Bar
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (BOOL) prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
