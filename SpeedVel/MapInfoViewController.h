//
//  MapInfoViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 05/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Mapkit/Mapkit.h>
#import "AppDelegate.h"

@interface MapInfoViewController : UIViewController <UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate>
{
    AppDelegate *appDelegate;
    UIImagePickerController *imgPickerPhoto;
    NSURL *pathImage;
}
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) IBOutlet UITextField *txfName;
@property (nonatomic, strong) IBOutlet UITextView *txvDescription;
@property (nonatomic, strong) IBOutlet UIImageView *imgPinBack;

- (IBAction)onClickBarButtons:(id)sender;

@end
