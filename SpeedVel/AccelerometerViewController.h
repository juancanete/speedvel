//
//  AccelerometerViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 30/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreMotion/CoreMotion.h"

#define kFrecuencyAccelerometer 10
#define KCenterImagePoint 40

@interface AccelerometerViewController : UIViewController
{
    CMMotionManager *motionManager, *motionMButton;
    
    float xMainRelative, yMainRelative;
}

@property (nonatomic, retain) IBOutlet UIImageView *imgPoint;

- (IBAction) onClickInitPosition;

@end
