//
//  FirstViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 02/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "SpeedViewController.h"

@interface SpeedViewController ()

@end

@implementation SpeedViewController

//Main View
@synthesize lblSpeed;
@synthesize lblAcceleration;
@synthesize bbtnStart;
@synthesize pgrThrottle;
@synthesize pgrBrake;
@synthesize lblMeasure;
@synthesize nviMainScreen;
@synthesize vwContainer;

#pragma mark - AdBannerDelegate
- (void) bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"show banner");
    [self adjustBannerView];
}

- (void) bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Error!!!!: %@", [error description]);
    [self adjustBannerView];
}

#pragma mark - Methods

float Degrees2RadiansFor(float degrees)
{
    return degreesToRadians(degrees) * 0.9;
}

- (void) rotateImageNeedle: (float) speed
{
    //if (speedTest <= 280) {
    //  speedTest += 1;
    imgNeedle.transform = CATransform3DMakeRotation(Degrees2RadiansFor(speed + kStartNeedle), 0, 0, 1);
    
    //}else
    //{
    //    [timerSpeed invalidate];
    //}
    
    
}

- (void) updateElementsView
{
    [lblSpeed setText:appDelegate.stSpeed];
    [lblAcceleration setText:appDelegate.stAcceleration];
    [pgrThrottle setProgress:appDelegate.flThrottle];
    [pgrBrake setProgress:appDelegate.flBrake];
    [self rotateImageNeedle:appDelegate.flSpeed];
    
}

- (void) initAllElements
{
    //Hide GPS Info Screen
    if (appDelegate.vwcGPSInfo.view.alpha == 1.0) {
        [UIView animateWithDuration:0.4 animations:^{
            [appDelegate.vwcGPSInfo.view setAlpha:0.0];
        }];
    }
    
    [lblAcceleration setText:@"0.00"];
    [lblSpeed setText:@"0.0"];
    [pgrBrake setProgress:0.0 animated:YES];
    [pgrThrottle setProgress:0.0 animated:YES];
    [self rotateImageNeedle:kStartNeedle];
}

#pragma mark - iAdBanner Methods

- (void) createAdBannerView
{
    bannerView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    CGRect bannerFrame = bannerView.frame;
    bannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.height;//self.view.frame.size.height;
    bannerView.frame = bannerFrame;
    
    //self.canDisplayBannerAds = YES;
    
    //Delegate
    [bannerView setDelegate:self];
}

- (void) adjustBannerView
{
    //CGRect contentView = self.vwContainer.frame;
    //float y1 = 0;
    //float y = 0;
    CGRect adBannerFrame = bannerView.frame;
    
    NSLog(@"kOriginalOrigen: %f",kOriginalOrigen);
    NSLog(@"kHeightBanner: %f",kHeightBanner);
    NSLog(@"kHeightTabBar: %f",kHeightTabBar);
    
    if ([bannerView isBannerLoaded]) {
        
        //contentView.origin.y = kOriginalOrigen - kHeightBanner;
        //y1 = [[UIScreen mainScreen] bounds].size.height - kHeightTabBar - kHeightBanner;
        //y = kOriginalOrigen - kHeightBanner;
        
        if ([appDelegate isiOS7]) {
            adBannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.height - kHeightBanner;
        }else
        {
            adBannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.height - kHeightBanner - kHeightStatusBar;
        }
        
    }else
    {
        //contentView.origin.y = kOriginalOrigen;
        adBannerFrame.origin.y = [[UIScreen mainScreen] bounds].size.height;
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        //self.vwContainer.frame = contentView;
        bannerView.frame = adBannerFrame;
    }];
}

#pragma mark - IBActions

- (IBAction) onClickStart:(id)sender
{
    if (![sender isKindOfClass:[UIBarButtonItem class]]) {
        return;
    }
    
    int tag = [(UIBarButtonItem *) sender tag];
    
    [UIView animateWithDuration:0.4 animations:^{
        [appDelegate.vwcGPSInfo.view setAlpha:1.0];
    }];
    
    switch (tag) {
        case 0:
            
            //Start updating location
            [appDelegate setupLocationManager];
            
            //Change Title and tag
            [bbtnStart setTitle:@"Stop"];
            [bbtnStart setTag:1];
            
            //Check and update values
            timerValues = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateElementsView) userInfo:nil repeats:YES];
            
            //Call to acceleration
            appDelegate.bIsGPSActivated = YES;
            [appDelegate accelerationSlider];
            
            break;
        case 1:
            
            
            //Stop updating location
            [appDelegate stopLocation];
            
            //Stop timer
            [timerValues invalidate];
            
            //Change Title and tag
            [bbtnStart setTitle:@"Start"];
            [bbtnStart setTag:0];
            
            //Init elements
            [self initAllElements];
            
            break;
            
        default:
            break;
    }
}

#pragma mark - Rotation View Controller

/*- (void) awakeFromNib
{
    isShowingLandscapeView = NO;
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
}
*/
- (void)orientationChanged:(NSNotification *)notification
{
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    if (UIDeviceOrientationIsLandscape(deviceOrientation) && !isShowingLandscapeView) {
        [self performSegueWithIdentifier:@"LandscapeSpeedView" sender:self];
        isShowingLandscapeView = YES;
        
    }else if (UIDeviceOrientationIsPortrait(deviceOrientation) && isShowingLandscapeView)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        isShowingLandscapeView = NO;
    }
}

#pragma mark - View Life Cycle

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initAllElements];
    
    //Set point rotating image
    //imgNeedle.center = CGPointMake(210, 190);
    imgNeedle = [CALayer layer];
    imgNeedle.contents = (__bridge id)([UIImage imageNamed:@"velocimetro-aguja2.png"].CGImage);
    
    //Needle
    imgNeedle.frame = CGRectMake(140, 190, 40, 20);
    imgNeedle.bounds = CGRectMake(0, 0, CGImageGetWidth((__bridge CGImageRef)imgNeedle.contents) + 15, CGImageGetHeight((__bridge CGImageRef)imgNeedle.contents));
    
    imgNeedle.anchorPoint = CGPointMake(0.85, 0.5);
    imgNeedle.transform = CATransform3DMakeRotation(Degrees2RadiansFor(kStartNeedle), 0, 0, 1);
    //Insert layer to Superview
    [self.view.layer addSublayer:imgNeedle];
    
    //Slide Menu
    // Change button color
    //_sidebarButton.tintColor = [UIColor colorWithWhite:0.96f alpha:0.2f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    //Landscape Screen
    isShowingLandscapeView = NO;
    
    //AppDelegate
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //Acceleration UISlider
    appDelegate.bIsV0 = NO;
    appDelegate.bIsGPSActivated = NO;
    
    //Init view properties
    //[nvbMainScreen setTintColor:[UIColor colorWithRed:0.0627451 green:0.22745098 blue:0.67058824 alpha:1]];
    
    //ProgressView
    CGAffineTransform transform;
    
    if ([appDelegate isiOS7]) {
        transform = CGAffineTransformMakeScale(1, 6);
        
    }else
    {
        transform = CGAffineTransformMakeScale(1, 1.25);
    }
    
    pgrThrottle.transform = transform;
    pgrBrake.transform = transform;
    
    //UINavigationBar
    if (![appDelegate isiOS7]) {
        //[nvbMainScreen setFrame:CGRectMake(0, 0, nvbMainScreen.frame.size.width, nvbMainScreen.frame.size.height)];
    }
    
    //Labels
    UIFont *fontSpeed = [UIFont fontWithName:@"DS-Digital" size:40];
    [lblSpeed setFont:fontSpeed];
    UIFont *fontGForce = [UIFont fontWithName:@"DS-Digital" size:40];
    [lblAcceleration setFont:fontGForce];
    
    //InfoGPSViewController
    appDelegate.vwcGPSInfo = [[GPSInfoViewController alloc] init];
    
    [appDelegate.vwcGPSInfo.view setFrame:CGRectMake(0, 0, appDelegate.vwcGPSInfo.view.frame.size.width, appDelegate.vwcGPSInfo.view.frame.size.height)];
    [self.view bringSubviewToFront:appDelegate.vwcGPSInfo.view];
    [appDelegate.vwcGPSInfo.view setAlpha:0.0];
    [self.view addSubview:appDelegate.vwcGPSInfo.view];
    
    //Banner Configuration
    [self createAdBannerView];
    [self.view addSubview:bannerView];
    
    // Check if there is authorization to use location
    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied
            || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"SpeedVel" message:NSLocalizedString(@"REACTIVATELOC", @"Location deactivated") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle no, thanks button
                                       }];
            
            [alert addAction:okButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    //Show Km/h or mph
    if ([appDelegate isKmhOrMph]) {
        //Miles
        [lblMeasure setText:@"mph"];
    }else
    {
        //km/h
        [lblMeasure setText:@"km/h"];
    }
    
    //Start GPS automatically
    if ([appDelegate isStartGPSAutomatically]) {
        [bbtnStart setEnabled:NO];
        [self onClickStart:bbtnStart];
    }else
    {
        [bbtnStart setEnabled:YES];
    }
    
    //Landscape
    if (!isShowingLandscapeView) {
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    
    if (bannerView != nil) {
        [self adjustBannerView];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    if (appDelegate.bIsGPSActivated && !isShowingLandscapeView) {
        //Simulate push stop button
        [self onClickStart:bbtnStart];
    }
    
    //Show landscape view
    if (!isShowingLandscapeView) {
        //Delete Observer
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
    //Banner disappear
    /*if (bannerView != nil) {
        self.canDisplayBannerAds = NO;
    }*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
