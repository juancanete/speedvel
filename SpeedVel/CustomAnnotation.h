//
//  customAnnotation.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 05/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface CustomAnnotation : NSObject

@property (nonatomic,copy) NSString *title;
@property (nonatomic, copy) NSString *stDescription;
@property (nonatomic, strong) UIImageView *imgPhoto;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title description: (NSString *) description image: (NSURL *) urlImage;

@end
