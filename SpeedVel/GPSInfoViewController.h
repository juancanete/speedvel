//
//  GPSInfoViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 12/10/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"

@interface GPSInfoViewController : UIViewController
{
    
}

@property (nonatomic, retain) IBOutlet UILabel *lblInfoLabel;
@property (nonatomic, retain) IBOutlet UIImageView *imgBackBlack;

@end
