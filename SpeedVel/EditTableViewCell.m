//
//  EditTableViewCell.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 09/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import "EditTableViewCell.h"

@implementation EditTableViewCell

@synthesize lblName;
@synthesize lblDescription;
@synthesize imgPhoto;
@synthesize btnEdit;
@synthesize txfName;
@synthesize txvDescription;
@synthesize bIsSelectedEdit;
@synthesize viewController;
@synthesize btnFacebook;
@synthesize btnTwitter;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Methods

/*- (IBAction)onClickEdit
{
    if (!bIsSelectedEdit) {
        [lblName setHidden:YES];
        [txfName setHidden:NO];
        [lblDescription setHidden:YES];
        [txvDescription setHidden:NO];
        bIsSelectedEdit = YES;
    }else
    {
        [lblName setHidden:NO];
        [txfName setHidden:YES];
        [lblDescription setHidden:NO];
        [txvDescription setHidden:YES];
        bIsSelectedEdit = NO;
    }
}*/

- (IBAction)onClickSocial:(id)sender
{
    if (![sender isKindOfClass:[UIButton class]]) {
        return;
    }
    
    NSInteger tag = [(UIButton *) sender tag];
    
    switch (tag) {
        case 0:
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                SLComposeViewController *twitter = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                [twitter setInitialText:@"#SpeedVelAPP "];
                [twitter addImage:imgPhoto.image];
                
                [self.viewController presentViewController:twitter animated:YES completion:nil];
            }
            
            break;
        case 1:
            
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                SLComposeViewController *facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [facebook setInitialText:@"#SpeedVelAPP "];
                [facebook addImage:imgPhoto.image];
                
                [self.viewController presentViewController:facebook animated:YES completion:nil];
            }
            
            break;
            
        default:
            break;
    }
}

- (void) setImagePin: (NSURL *) url
{
    ALAssetsLibraryAssetForURLResultBlock resultBlock = ^(ALAsset *myasset){
        CGImageRef iref = [myasset thumbnail];
        
        if (iref) {
            UIImage *imgThumb = [UIImage imageWithCGImage:iref];
            [self.imgPhoto setImage:imgThumb];
        }
    };
    
    ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
    {
        NSLog(@"booya, cant get image - %@",[myerror localizedDescription]);
    };
    
    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
    [assetsLibrary assetForURL:url resultBlock:resultBlock failureBlock:failureblock];
}

@end
