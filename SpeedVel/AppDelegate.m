//
//  AppDelegate.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 02/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "AppDelegate.h"

@implementation UITabBarController (RotationNone)

- (BOOL) shouldAutorotate
{
    if (self.selectedViewController) {
        return [self.selectedViewController shouldAutorotate];
    }else
    {
        return YES;
    }
}

- (NSUInteger) supportedInterfaceOrientations
{
    if (self.selectedViewController) {
        return [self.selectedViewController supportedInterfaceOrientations];
    }else
    {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
}

@end

@implementation AppDelegate

//CoreData variables
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

//App variables
@synthesize intPinID;
@synthesize vwGPSSignal;
@synthesize bIsV0;
@synthesize flThrottle;
@synthesize flBrake;
@synthesize stAcceleration;
@synthesize stSpeed;
@synthesize flSpeed;
@synthesize bIsGPSActivated;

//MapView Datas
@synthesize mArrPins;

//Settings Screen
@synthesize arrContentFile;
@synthesize imgBackgroundLand;

//InfoGPS
@synthesize vwcGPSInfo;

#pragma mark - iOS Version

- (BOOL) isiOS7
{
    if ([[[UIDevice currentDevice] systemVersion] hasPrefix:@"7."]) {
        return YES;
    }else
    {
        return NO;
    }
}

- (BOOL) isiPhone4
{
    if ([[UIScreen mainScreen] bounds].size.height < 568) {
        return YES;
    }else{
        return NO;
    }
}

#pragma mark - CLLocationManagerDelegate

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //Calculate Acceleration
    [self accelerationSlider];
    
    //Calculate Speed
    if (manager.location.speed < 0.0) {
        //Speed is negative;
        stSpeed = @"0.0";
        flSpeed = 0.0;
    }else
    {
        if (vwcGPSInfo.view.alpha == 1.0) {
            //Hide vwcGPSInfo
            [UIView animateWithDuration:0.4 animations:^{
                [vwcGPSInfo.view setAlpha:0.0];
            } completion:^(BOOL finished){
                //Speed is zero or higher
                stSpeed = [NSString stringWithFormat:@"%.1f",(manager.location.speed) * [self measurementMilesOrKilometer]];
                flSpeed = (manager.location.speed) * [self measurementMilesOrKilometer];
            }];
        }else
        {
            //Speed is zero or higher
            stSpeed = [NSString stringWithFormat:@"%.1f",(manager.location.speed) * [self measurementMilesOrKilometer]];
            flSpeed = (manager.location.speed) * [self measurementMilesOrKilometer];
        }
    }
    
    stError = [manager.location description];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //[lblError setText:[error description]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Speed Error" message: NSLocalizedString(@"MESSAGE", @"Info message") delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
    [alert show];
}

#pragma mark Acceleration Methods

- (float) calculateAcceleration: (float) initSpeed secondSpeed:(float) finalSpeed time: (NSTimeInterval) timeInterval
{
    return (finalSpeed - initSpeed)/timeInterval;
}

- (void) accelerationSlider
{
    if (!bIsV0) {
        //Get init speed
        bIsV0 = YES;
        fV0 = locManager.location.speed;
        dtTime0 = [NSDate date];
    }else
    {
        //Get final Speed and calculate acceleration
        fV1 = locManager.location.speed;
        dtTime1 = [NSDate date];
        
        float fAcceleration = [self calculateAcceleration:fV0 secondSpeed:fV1 time:[dtTime1 timeIntervalSinceDate:dtTime0]];
        
        NSLog(@"Time Interval: %f",[dtTime1 timeIntervalSinceDate:dtTime0]);
        
        if ((fAcceleration/kGForce) >= 0) {
            flThrottle = (fAcceleration/kGForce)/kMaxThrottle;
            flBrake = 0.0;
        }else
        {
            flThrottle = 0.0;
            flBrake = fabsf((fAcceleration/kGForce)/kMaxBrake);
        }
        
        
        stAcceleration = [NSString stringWithFormat:@"%.2f",fAcceleration];
        bIsV0 = NO;
    }
}

- (float) measurementMilesOrKilometer
{
    NSNumber *nbMeasurment = [arrContentFile objectAtIndex:1];
    
    //Show miles or kilometer in speedometer
    if ([nbMeasurment boolValue]) {
        
        return 2.236936;
    }else{
        return 3.6;
    }
}

- (BOOL) isKmhOrMph
{
    NSNumber *nbMilesOrKm = [arrContentFile objectAtIndex:1];
    
    return [nbMilesOrKm boolValue];
}

- (BOOL) isStartGPSAutomatically
{
    NSNumber *nbStartGPS = [arrContentFile objectAtIndex:0];
    
    return [nbStartGPS boolValue];
}

#pragma - Methods Location

- (void) setupLocationManager
{
    //Create Manager Object
    locManager = [[CLLocationManager alloc] init];
    [locManager setDelegate:self];
    [locManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
    
    //Once configurated all setting, the location manager must "start location"
    [locManager startUpdatingLocation];
    bIsGPSActivated = YES;
}

- (void) stopLocation
{
    //Stop acceleration proccess
    bIsGPSActivated = NO;
    
    [locManager stopUpdatingLocation];
}

#pragma mark - Methods AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    //Init main variables
    bIsGPSActivated = NO;
    
    //This disables the autosleep
    application.idleTimerDisabled = YES;
    
    
    //Init Pin Array with Core Data function
    //mArrPins = [[NSMutableArray alloc] init];
    [self findAllPins];
    
    
    //Delete items CoreData
    //[self deleteAllObjects:@"Places"];
    
    //Load or create the file
    NSArray *arrPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSFileManager *flManager = [NSFileManager defaultManager];
    NSString *pathFile = [[arrPath objectAtIndex:0] stringByAppendingPathComponent:@"MainSettings2.plist"];
    NSString *pathImage = [[arrPath objectAtIndex:0] stringByAppendingPathComponent:@"ImageDocument.plist"];
    
    if (([[NSFileManager defaultManager] fileExistsAtPath:pathFile])) {
        arrContentFile = [[NSMutableArray alloc] initWithContentsOfFile:pathFile];
        intPinID =  [arrContentFile[2] intValue];
        NSLog(@"object 0: %@ object 1: %@",[arrContentFile objectAtIndex:0], [arrContentFile objectAtIndex:1]);
    }else
    {
        
        NSData *datFile = [[NSData alloc] init];
        [flManager createFileAtPath:[[arrPath objectAtIndex:0] stringByAppendingPathComponent:@"MainSettings2.plist"] contents:datFile attributes:nil];
        
        
        /*
         arrContentFile[0] => GPS
         arrContentFile[1] => Measurement
         arrContentFile[2] => pinID
         */
        
        intPinID = 0;
        arrContentFile = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],[NSNumber numberWithInt:intPinID], nil];
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathImage]){
        imgBackgroundLand = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:pathImage]];
    }else
    {
        [flManager createFileAtPath:[[arrPath objectAtIndex:0] stringByAppendingPathComponent:@"ImageDocument.plist"] contents:UIImagePNGRepresentation(imgBackgroundLand.image) attributes:nil];
    }
    
    //Delay to show launch image
    sleep(1);
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSArray *arrPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSFileManager *flManager = [NSFileManager defaultManager];
    NSString *pathFile = [[arrPath objectAtIndex:0] stringByAppendingPathComponent:@"MainSettings2.plist"];
    NSString *pathImage = [[arrPath objectAtIndex:0] stringByAppendingPathComponent:@"ImageDocument.plist"];
    //NSError *error;
    
    
    //Delete items and create with new datas
    /*if (([flManager removeItemAtPath:pathFile error:&error] == YES) && ([flManager removeItemAtPath:pathImage error:&error] == YES)) {
        NSData *datFile = [[NSData alloc] init];
        NSData *datImage = UIImagePNGRepresentation(imgBackgroundLand.image);
        
        [flManager createFileAtPath:pathFile contents:datFile attributes:nil];
        [flManager createFileAtPath:pathImage contents:datImage attributes:nil];
        
        [arrContentFile writeToFile:pathFile atomically:YES];
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Ha ocurrido un error al borrar el fichero. Cierre la aplicación y vuelva a abrirla" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles: nil];
        [alert show];
    }*/
    [arrContentFile removeObjectAtIndex:2];
    [arrContentFile addObject:[NSNumber numberWithInt:intPinID]];
    
    [arrContentFile writeToFile:pathFile atomically:YES];
    [flManager createFileAtPath:pathImage contents:UIImagePNGRepresentation(imgBackgroundLand.image) attributes:nil];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

//Find Contact CoreData
- (void)findAllPins {
    
    NSManagedObjectContext *context =
    [self managedObjectContext];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Places"
                inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    //NSPredicate *pred =
    //[NSPredicate predicateWithFormat:@"(name = %@)",
    // _name.text];
    //[request setPredicate:pred];
    //NSManagedObject *matches = nil;
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    mArrPins = [[NSMutableArray alloc] init];
    
    if ([objects count] > 0) {
        for (int i = 0; i < [objects count];i++) {
            NSArray *keys = [[[objects[i] entity] attributesByName] allKeys];
            NSDictionary *dict = [objects[i] dictionaryWithValuesForKeys:keys];
            [mArrPins addObject:dict];
        }
    }
    /*if ([objects count] == 0) {
     //_status.text = @"No matches";
     } else {
     matches = objects[0];
     _address.text = [matches valueForKey:@"address"];
     _phone.text = [matches valueForKey:@"phone"];
     _status.text = [NSString stringWithFormat:
     @"%lu matches found", (unsigned long)[objects count]];
     }*/
}

- (void) deleteAllObjects: (NSString *) entityDescription  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
    	[_managedObjectContext deleteObject:managedObject];
    	NSLog(@"%@ object deleted",entityDescription);
    }
    if (![_managedObjectContext save:&error]) {
    	NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"SpeedVelModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"SpeedVel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
