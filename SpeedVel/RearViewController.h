//
//  RearViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 03/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"
#import "SWRevealViewController.h"

@interface RearViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *arrElementsCell;
}

@property (nonatomic, strong) IBOutlet UITableView *tblMenu;

@end
