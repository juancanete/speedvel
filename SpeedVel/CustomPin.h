//
//  CustomPin.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 05/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPin : UIView

@property (weak, nonatomic) IBOutlet UILabel *calloutLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIImageView * imgPhoto;


@end
