//
//  AppDelegate.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 02/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreLocation/CoreLocation.h"
//#import "GADBannerView.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "GPSInfoViewController.h"

#define kGForce 9.81
#define kMaxThrottle 3.0
#define kMaxBrake 5.0
#define kHeightStatusBar 20.0
#define kHeightTabBar 49.0
#define kHeightBanner 50.0
#define kOriginalOrigen ([[[UIDevice currentDevice] systemVersion] hasPrefix:@"7."] ? 64.0 : 44.0)

@interface UINavigationController(RotationNone)

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
{
    //SpeedViewController
    //Information
    //NSString *stSpeed;
    NSString *stError;
    //NSString *stAcceleration;
    
    //Location
    CLLocationManager *locManager;
    
    //Calculate Acceleration
    float fV0,fV1;
    NSDate *dtTime0, *dtTime1;
}

//CoreData variables and functions
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

//App Variables
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIView *vwGPSSignal;
@property (nonatomic) NSInteger intPinID;

//Information
@property (nonatomic) float flThrottle;
@property (nonatomic) float flBrake;
@property (nonatomic, strong) NSString *stSpeed;
@property (nonatomic) float flSpeed;
@property (nonatomic, strong) NSString *stAcceleration;

//InfoGPSViewController
@property (nonatomic, strong) GPSInfoViewController *vwcGPSInfo;

//Acceleration featured
@property (nonatomic) BOOL bIsV0;
@property (nonatomic) BOOL bIsGPSActivated;

//MapViewDatas
@property (nonatomic, strong) NSMutableArray *mArrPins;

//Settings Screen
@property (nonatomic,strong) NSMutableArray *arrContentFile;
@property (nonatomic,strong) UIImageView *imgBackgroundLand;

//Detect iOS Version
- (BOOL) isiOS7;
- (BOOL) isiPhone4;

//Methods Location
- (void) setupLocationManager;
- (void) stopLocation;

//Acceleration Methods
- (float) calculateAcceleration: (float) initSpeed secondSpeed:(float) finalSpeed time: (NSTimeInterval) timeInterval;
- (void) accelerationSlider;
- (float) measurementMilesOrKilometer;
- (BOOL) isKmhOrMph;
- (BOOL) isStartGPSAutomatically;


@end
