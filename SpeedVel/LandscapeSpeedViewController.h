//
//  LandscapeSpeedViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 19/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreMotion/CoreMotion.h>
#import <QuartzCore/QuartzCore.h>
#import "iAd/iAd.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

#define kAccelerometerFrequency 10.0
#define kLandOriginalOrigin 0
#define kHeightBannerAdmob 32
#define kDifferenceResolution 88

//Frame Images
#define kCenterImages (([[UIScreen mainScreen] bounds].size.height) < 568 ? 240:284)
#define kResizeImages (([[UIScreen mainScreen] bounds].size.height) < 568 ? 60:0)

const NSString *idUniqueAds = @"ca-app-pub-7648066946763999/3142478262";

@interface LandscapeSpeedViewController : UIViewController <GADBannerViewDelegate>
{
    //Timer to update values
    NSTimer *timerValues;
    
    //appDelegate
    AppDelegate *appDelegate;
    //float speed;
    NSTimer *timerSpeed;
    CALayer *imgNeedle;
    
    //Accelerometer
    CMMotionManager *motionManager;
    
    //ADBannerView
    ADBannerView *bannerLandView;
    
    //Admob Banner
    GADBannerView *bannerAdmob;
}

//Information View
@property (nonatomic, retain) IBOutlet UILabel *lblSpeed;
@property (nonatomic, retain) IBOutlet UIView *vwOriginal;
//@property (nonatomic, retain) IBOutlet UIButton *btnStart;
@property (nonatomic, retain) IBOutlet UILabel *lblMeasure;
@property (nonatomic, retain) IBOutlet UIProgressView *pgrThrottle;
@property (nonatomic, retain) IBOutlet UIProgressView *pgrBrake;
@property (nonatomic, retain) IBOutlet UIImageView *imgLandscapeBack;
@property (nonatomic, retain) IBOutlet UIImageView *imgSpeedometer;
@property (nonatomic, retain) IBOutlet UIImageView *imgAnalogSpeedo;
@property (nonatomic, retain) IBOutlet UIImageView *imgDigitalSpeedo;
@property (nonatomic, retain) IBOutlet UIView *vwContainerView;

//Methods
- (void) updateElementsView;
- (void) initAllElements;

@end
