//
//  ImageCellView.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 15/08/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "QuartzCore/QuartzCore.h"

@interface ImageCellView : UITableViewCell <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    AppDelegate *appDelegate;
    UIImagePickerController *imgPickerPhoto;
}

@property (nonatomic, retain) IBOutlet UIImageView *imgBackSpeedo;
@property (nonatomic, retain) IBOutlet UIImageView *imgShadow;

- (void) setImageBack;
- (void) setFeaturesImageView;

@end
