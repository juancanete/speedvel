//
//  EditPinViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 09/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "EditTableViewCell.h"


#define kHeightMinCell 86
#define kHeightMaxCell 170

@interface EditPinViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate *appDelegate;
    NSInteger selectedIndex;
}

-(IBAction)onClickBack;

@property (nonatomic, strong) IBOutlet UITableView *tblPins;

@end
