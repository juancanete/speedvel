//
//  GPSInfoViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 12/10/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "GPSInfoViewController.h"

@interface GPSInfoViewController ()

@end

@implementation GPSInfoViewController

@synthesize lblInfoLabel;
@synthesize imgBackBlack;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Set Preferences
    [imgBackBlack.layer setCornerRadius:5.0];
    [imgBackBlack setAlpha:0.8];
    [lblInfoLabel setText:NSLocalizedString(@"INFOGPS", @"Information about GPS Signal")];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
