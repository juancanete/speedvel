//
//  MapViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 03/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import <MapKit/MapKit.h>
#import "CustomAnnotation.h"
#import "CustomPin.h"
#import "AppDelegate.h"
#import "MapInfoViewController.h"

#define kOriginalOrigin 0
#define kHeightBannerAdmobPort 32



@interface MapViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    BOOL bIsZoomPushed;
    BOOL bIsAddPushed;
    
    //AppDelegate
    AppDelegate *appDelegate;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) IBOutlet UIButton *btnZoom;
@property (nonatomic, strong) IBOutlet UIButton *btnEditPin;

- (IBAction)onClickLocation: (id) sender;

@end
