//
//  MapViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 03/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

@synthesize mapView;
@synthesize btnEditPin;
@synthesize btnZoom;

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id)annotation {
    
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *identifier = @"myAnnotation";
    MKAnnotationView * annotationView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (!annotationView)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        [annotationView setImage:[UIImage imageNamed:@"pin.png"]];
    }else {
        annotationView.annotation = annotation;
    }
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if(![view.annotation isKindOfClass:[MKUserLocation class]]) {
        CustomPin *customView = (CustomPin *)[[[NSBundle mainBundle] loadNibNamed:@"customView" owner:self options:nil] objectAtIndex:0];
        CGRect customViewFrame = customView.frame;
        customViewFrame.origin = CGPointMake(-customViewFrame.size.width/2 + 15, -customViewFrame.size.height);
        customView.frame = customViewFrame;
        [customView.calloutLabel setText:[(CustomAnnotation*)[view annotation] title]];
        [customView.lblDescription setText:[(CustomAnnotation *)[view annotation] stDescription]];
        [customView.imgPhoto setImage:[[(CustomAnnotation *)[view annotation] imgPhoto] image]];
        [view addSubview:customView];
    }
    
}


-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    for (UIView *subview in view.subviews ){
        [subview removeFromSuperview];
    }
}

#pragma mark - CLLocationManager
- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations objectAtIndex:[locations count] - 1];
    
    if (bIsZoomPushed) {
        
        CLLocationCoordinate2D zoomLocation = currentLocation.coordinate;
        
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.3, 0.3);
        [self.mapView setRegion:viewRegion animated:YES];
        bIsZoomPushed = NO;
    }
    
    if (bIsAddPushed) {
        MapInfoViewController *mapInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"mapInfo"];
        [mapInfo setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        mapInfo.coordinate = currentLocation.coordinate;
        
        [self presentViewController:mapInfo animated:YES completion:nil];
        
        bIsAddPushed = NO;
    }
    
    NSLog(@"Current Location:%f %f", currentLocation.coordinate.longitude, currentLocation.coordinate.latitude);
    
    
    [UIView animateWithDuration:0.4 animations:^{
        [appDelegate.vwcGPSInfo.view setAlpha:0.0];
    }];
    
    [manager stopUpdatingLocation];
}

#pragma mark - Manage Pins

- (IBAction)onClickLocation: (id) sender
{
    int tag = [sender tag];
    
    switch (tag) {
        case 0:
            bIsAddPushed = YES;
            break;
        case 1:
            bIsZoomPushed = YES;
            break;
        default:
            break;
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        [appDelegate.vwcGPSInfo.view setAlpha:1.0];
        [locationManager startUpdatingLocation];
    }];
}

#pragma mark - Lifecycle
- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void) viewDidAppear:(BOOL)animated
{
    //Show location
    [self.mapView setShowsUserLocation:YES];
    NSLog(@"Mapa: %@",[self.mapView description]);
    for (NSDictionary *dictPin in appDelegate.mArrPins) {
        CLLocationCoordinate2D coordinate;
        
        NSNumber *coord;
        coord = (NSNumber *)[dictPin objectForKey:@"coordLatPin"];
        double lat = [coord doubleValue];
        coord = [dictPin objectForKey:@"coordLongPin"];
        double longitude = [coord doubleValue];
        
        coordinate.latitude = lat;
        coordinate.longitude = longitude;
        
        NSString *stDescript = [dictPin objectForKey:@"descPin"];
        NSString *stImagePin = [dictPin objectForKey:@"imgPin"];
        NSURL *urlImage = [[NSURL alloc] initWithString:stImagePin];
        
        CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:coordinate title:[dictPin objectForKey:@"namePin"] description:stDescript image:urlImage];
        
        //[mArrAnnotations addObject:annotation];
        
        [self.mapView addAnnotation:(id)annotation];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    NSLog(@"Annotations on map: %i",[self.mapView.annotations count]);
    
    //Hide location
    [self.mapView setShowsUserLocation:NO];
    
    NSMutableArray *mArrRemAnnot = [NSMutableArray arrayWithCapacity:[[self.mapView annotations] count]];
    
    for (id annotation in [self.mapView annotations]) {
        if ([annotation isKindOfClass:[CustomAnnotation class]]) {
            [mArrRemAnnot addObject:annotation];
        }
    }
    
    
    [self.mapView removeAnnotations:mArrRemAnnot];
    
    mArrRemAnnot = nil;
    //[mArrAnnotations removeAllObjects];
    
    NSLog(@"Annotations on map: %i",[self.mapView.annotations count]);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Init AppDelegate
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    //If relocate buttons if the phone is an iPhone 4/4s
    if ([appDelegate isiPhone4]) {
        [btnZoom setFrame:CGRectMake(btnZoom.frame.origin.x, btnZoom.frame.origin.y - 88, btnZoom.frame.size.width, btnZoom.frame.size.height)];
        [btnEditPin setFrame:CGRectMake(btnEditPin.frame.origin.x, btnEditPin.frame.origin.y - 88, btnEditPin.frame.size.width, btnEditPin.frame.size.height)];
    }
    
    //Init Array
    //mArrAnnotations = [[NSMutableArray alloc] init];
    
    bIsZoomPushed = NO;
    bIsAddPushed = NO;
    
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    //Init CLLocationManager
    locationManager = [[CLLocationManager alloc] init];
    
    [locationManager setDelegate:self];
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    //Set Delegate of the MapView
    [self.mapView setDelegate:self];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
