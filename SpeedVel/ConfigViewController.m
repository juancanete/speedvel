//
//  SecondViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 02/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "ConfigViewController.h"

@interface ConfigViewController ()

@end

@implementation ConfigViewController

@synthesize tblSettings;
@synthesize nvSettings;
@synthesize tbiSettings;

#pragma - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *stIdentifier;
    
    switch (indexPath.row) {
        case 0:
            stIdentifier = @"tableCellGPS";
            break;
        case 1:
            stIdentifier = @"tableCellMeasure";
            break;
        case 2:
            stIdentifier = @"tableCellImage";
            break;
        default:
            break;
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:stIdentifier];
    
    return cell.frame.size.height;
}

#pragma - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GPSCellView *cell;
    MeasureCellView *cellMeasure;
    ImageCellView *cellImage;
    
    switch (indexPath.row) {
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:@"tableCellGPS"];
            [cell.swtGPS setOn:[appDelegate isStartGPSAutomatically]];
            
            return cell;
            
            break;
        case 1:
            cellMeasure = [tableView dequeueReusableCellWithIdentifier:@"tableCellMeasure"];
            
            if (![appDelegate isiOS7]) {
                cellMeasure.sgmMeasure.transform = CGAffineTransformMakeScale(1, 0.75);
            }
            
            
            if (![appDelegate isKmhOrMph]) {
                [cellMeasure.sgmMeasure setSelectedSegmentIndex:0];
            }else
            {
                [cellMeasure.sgmMeasure setSelectedSegmentIndex:1];
            }
            
            return cellMeasure;
            
            break;
        case 2:
            cellImage = [tableView dequeueReusableCellWithIdentifier:@"tableCellImage"];
            [cellImage setFeaturesImageView];
            [cellImage setImageBack];
            
            return cellImage;
            break;
            
        default:
            break;
    }
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //Set image to main variable
    appDelegate.imgBackgroundLand = [[UIImageView alloc] initWithImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
    
    NSLog(@"Image: %@ and Info image: %@", appDelegate.imgBackgroundLand.image, [info objectForKey:UIImagePickerControllerOriginalImage]);
    //Dismiss picker view
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [tblSettings reloadData];
}

#pragma - Methods

- (IBAction)onClickSelectImage: (id) sender
{
    if (![sender isKindOfClass:[UIButton class]]) {
        return;
    }
    
    int tag = [(UIButton *) sender tag];
    
    switch (tag) {
        case 0:
            //appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            imgPickerPhoto = [[UIImagePickerController alloc] init];
            
            imgPickerPhoto.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [imgPickerPhoto setDelegate:self];
            
            [self presentViewController:imgPickerPhoto animated:YES completion:nil];
            
            break;
        case 1:
            appDelegate.imgBackgroundLand = nil;
            [tblSettings reloadData];
            break;
        default:
            break;
    }
}

- (IBAction)onClickBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma - View Life cycle

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //Slide Menu
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    //Init AppDelegate
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    //Init view properties
    [nvSettings setTintColor:[UIColor colorWithRed:0.0627451 green:0.22745098 blue:0.67058824 alpha:1]];
    [tbiSettings setTitle:NSLocalizedString(@"SETTINGS", @"Settings sections")];

    [tblSettings setBackgroundColor:[UIColor colorWithRed:0.8078431372549 green:0.8078431372549 blue:0.8078431372549 alpha:1]];
    
    //NavigationBar & UITableView
    if (![appDelegate isiOS7]) {
        [nvSettings setFrame:CGRectMake(0, 0, nvSettings.frame.size.width, nvSettings.frame.size.height)];
        [tblSettings setFrame:CGRectMake(0, kOriginalOrigen, tblSettings.frame.size.width, kHeightTableView)];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
