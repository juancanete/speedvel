//
//  Timer.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 11/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Timer : NSObject
{
    NSDate *start;
    NSDate *end;
}

- (void) startTimer;
- (void) stopTimer;
- (double) timeElapsedInSeconds;
- (double) timeElapsedInMilliseconds;
- (double) timeElapsedInMinutes;

@end
