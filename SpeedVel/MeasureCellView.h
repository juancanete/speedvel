//
//  MeasureCellView.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 15/08/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface MeasureCellView : UITableViewCell
{
    AppDelegate *appDelegate;
}

@property (nonatomic, retain) IBOutlet UISegmentedControl *sgmMeasure;

- (IBAction)onSelectMeasure;

@end
