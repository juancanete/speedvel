//
//  FirstViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 02/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreLocation/CoreLocation.h"
#import "AppDelegate.h"
#import "QuartzCore/QuartzCore.h"
#import "SWRevealViewController.h"
#import "iAd/iAd.h"

#define degreesToRadians(degrees)((degrees)/180.0 * M_PI)
#define kStartNeedle -40

@interface SpeedViewController : UIViewController <CLLocationManagerDelegate, ADBannerViewDelegate>
{
    //Booleans
    BOOL isShowingLandscapeView;
    
    //Timer check values
    NSTimer *timerValues;
    
    //AppDelegate
    AppDelegate *appDelegate;
    
    //Banner
    ADBannerView *bannerView;
    
    //Image needle
    CALayer *imgNeedle;
    
    //Test
    //float speedTest;
    //NSTimer *timerSpeed;
}

@property (nonatomic, retain) IBOutlet UILabel *lblSpeed;
@property (nonatomic, retain) IBOutlet UILabel *lblAcceleration;
@property (nonatomic, retain) IBOutlet UIProgressView *pgrThrottle;
@property (nonatomic, retain) IBOutlet UIProgressView *pgrBrake;
@property (nonatomic, retain) IBOutlet UILabel *lblMeasure;
@property (nonatomic, retain) IBOutlet UINavigationItem *nviMainScreen;
@property (nonatomic, retain) IBOutlet UIView *vwContainer;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *sidebarButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *bbtnStart;


//Methods
- (void) initAllElements;
- (void) updateElementsView;
- (void)orientationChanged:(NSNotification *)notification;

//Banner Methods
- (void) createAdBannerView;
- (void) adjustBannerView;

//IBActions
- (IBAction) onClickStart:(id)sender;

@end
