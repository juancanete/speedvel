//
//  EditPinViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 09/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import "EditPinViewController.h"

@interface EditPinViewController ()

@end

@implementation EditPinViewController

@synthesize tblPins;

#pragma - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedIndex == indexPath.row) {
        selectedIndex = -1;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        return;
    }
    
    if (selectedIndex >= 0) {
        NSIndexPath *previousIndex = [NSIndexPath indexPathForItem:selectedIndex inSection:0];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousIndex] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    selectedIndex = indexPath.row;
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (selectedIndex == indexPath.row) {
        return kHeightMaxCell;
    }
    
    return kHeightMinCell;
}

#pragma - UITableViewDataSource

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSLog(@"Index: %i",indexPath.row);
        
        //Delete from coredata
        NSNumber *pinID = [[appDelegate.mArrPins objectAtIndex:indexPath.row] objectForKey:@"pinID"];
        //NSNumber *pinID = [NSNumber numberWithInt:3];
        NSLog(@"Date: %i",[pinID intValue]);
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Places" inManagedObjectContext:context];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pinID==%i",[pinID intValue]];
        [fetchRequest setEntity:entity];
        [fetchRequest setPredicate:predicate];
        
        NSError *error;
        NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
        
        if ([items count] > 0) {
            NSManagedObject *object = items[0];
            
            [context deleteObject:object];
        }
        
        if (![context save:&error]) {
            NSLog(@"Error deleting %@",error);
        }
        
 //       [context deleteObject:item];
        
        //Delete from inner array
        [appDelegate.mArrPins removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellPin"];
    
    NSDictionary *dictPin = appDelegate.mArrPins[indexPath.row];
    [cell.lblName setText:[dictPin objectForKey:@"namePin"]];
    [cell.lblDescription setText:[dictPin objectForKey:@"descPin"]];
    NSString *stImagePin = [dictPin objectForKey:@"imgPin"];
    NSURL *urlImage = [[NSURL alloc] initWithString:stImagePin];
    [cell setImagePin:urlImage];
    [cell setBIsSelectedEdit:NO];
    [cell setViewController:self];
    
    if (selectedIndex == indexPath.row) {
        [cell.lblDescription setHidden:NO];
        [cell.btnTwitter setHidden:NO];
        [cell.btnFacebook setHidden:NO];
        // It's not working yet
        // [cell.btnEdit setHidden:NO];
    }
    
    if (cell == nil) {
        UITableViewCell *cellDefault = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
        return cellDefault;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [appDelegate.mArrPins count];
}

#pragma mark - Methods Action



-(IBAction)onClickBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - View lyfecycle

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [tblPins setBackgroundColor:[UIColor colorWithRed:0.8078431372549 green:0.8078431372549 blue:0.8078431372549 alpha:1]];
    [tblPins setAllowsMultipleSelection:NO];
    selectedIndex = -1;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
