//
//  GPSCellView.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 14/08/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface GPSCellView : UITableViewCell
{
    AppDelegate *appDelegate;
}

@property (nonatomic, retain) IBOutlet UISwitch *swtGPS;

- (IBAction)onSelectGPS;

@end
