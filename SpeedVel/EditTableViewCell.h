//
//  EditTableViewCell.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 09/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Social/Social.h>

@interface EditTableViewCell : UITableViewCell
{
    
}

@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *lblDescription;
@property (nonatomic, strong) IBOutlet UIImageView *imgPhoto;
@property (nonatomic, strong) IBOutlet UIButton *btnEdit;
@property (nonatomic, strong) IBOutlet UIButton *btnFacebook;
@property (nonatomic, strong) IBOutlet UIButton *btnTwitter;
@property (nonatomic, strong) UIViewController *viewController;
@property (nonatomic, strong) IBOutlet UITextField *txfName;
@property (nonatomic, strong) IBOutlet UITextView *txvDescription;
@property (nonatomic) BOOL bIsSelectedEdit;

- (void) setImagePin: (NSURL *) url;
- (IBAction)onClickSocial:(id)sender;
//- (IBAction)onClickEdit;

@end
