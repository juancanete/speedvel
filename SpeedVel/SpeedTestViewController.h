//
//  SpeedTestViewController.h
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 06/07/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreLocation/CoreLocation.h"
#import "AppDelegate.h"
#import "iAd/iAd.h"

@interface SpeedTestViewController : UIViewController <CLLocationManagerDelegate, ADBannerViewDelegate>
{
    CLLocationManager *locManager;
    NSTimer *timerChronometer;
    NSMutableString *stTimer;
    
    //Chronometer
    int miliseconds, seconds, minutes;
    NSMutableArray *arrSpeeds;
    AppDelegate *appDelegate;
    
    //Banner
    ADBannerView *bannerView;
}

@property (nonatomic, retain) IBOutlet UIView *vwContainer;
@property (nonatomic, retain) IBOutlet UINavigationBar *nvTimer;
@property (nonatomic, retain) IBOutlet UIButton *btnStartPause;
@property (nonatomic, retain) IBOutlet UIButton *btnStop;
@property (nonatomic, retain) IBOutlet UILabel *lblAverageSp;
@property (nonatomic, retain) IBOutlet UILabel *lblSpeed;
@property (nonatomic, retain) IBOutlet UILabel *lblSpeedMeasure;
@property (nonatomic, retain) IBOutlet UILabel *lblAveMearure;

//Time label
@property (nonatomic, retain) IBOutlet UILabel *lblTwoPoint1;
@property (nonatomic, retain) IBOutlet UILabel *lblTwoPoint2;
@property (nonatomic, retain) IBOutlet UILabel *lblMinutes;
@property (nonatomic, retain) IBOutlet UILabel *lblSeconds;
@property (nonatomic, retain) IBOutlet UILabel *lblMilliSeconds;

//Methods
- (void) chronometerLabel;
- (void) figureOutAverageSpeed:(NSMutableArray *) locations;

//IBActions
- (IBAction)onClickStartStop:(id)sender;

@end
