//
//  customAnnotation.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 05/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import "customAnnotation.h"

@implementation CustomAnnotation

-(id) initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title description: (NSString *) description image: (NSURL *) urlImage {
    
    self = [super init];
    
    if (self) {
        
        ALAssetsLibraryAssetForURLResultBlock resultBlock = ^(ALAsset *myasset){
            CGImageRef iref = [myasset thumbnail];
            
            if (iref) {
                UIImage *imgThumb = [UIImage imageWithCGImage:iref];
                self.imgPhoto= [[UIImageView alloc] initWithImage:imgThumb];
            }
        };
        
        ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
        {
            NSLog(@"booya, cant get image - %@",[myerror localizedDescription]);
        };
        
        self.coordinate =coordinate;
        self.title = [title copy];
        self.stDescription = [description copy];
        
        ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
        [assetsLibrary assetForURL:urlImage resultBlock:resultBlock failureBlock:failureblock];
        //self.imgPhoto = imageView;
    }
    return self;
}

-(id) init
{
    self = [super init];
    
    if (self) {
        
    }
    
    return self;
}


@end
