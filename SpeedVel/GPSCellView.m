//
//  GPSCellView.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 14/08/13.
//  Copyright (c) 2013 Juan Cañete Rodríguez. All rights reserved.
//

#import "GPSCellView.h"

@implementation GPSCellView

@synthesize swtGPS;

#pragma mark - Methods

- (IBAction)onSelectGPS
{
    //Check value AppDelegate
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //Init value NSNumber
    NSNumber *nbBool = [appDelegate.arrContentFile objectAtIndex:1];
    
    //Refresh values into appDelegate
    [appDelegate.arrContentFile removeAllObjects];
    appDelegate.arrContentFile = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:[swtGPS isOn]],nbBool, nil];
    
    NSLog(@"ArrContentFile: %i", [appDelegate.arrContentFile count]);
}

#pragma mark - View Life Cycle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
