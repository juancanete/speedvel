//
//  MapInfoViewController.m
//  SpeedVel
//
//  Created by Juan Cañete Rodríguez on 05/06/14.
//  Copyright (c) 2014 Juan Cañete Rodríguez. All rights reserved.
//

#import "MapInfoViewController.h"

@interface MapInfoViewController ()

@end

@implementation MapInfoViewController

@synthesize coordinate;
@synthesize txfName;
@synthesize txvDescription;
@synthesize imgPinBack;

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - UITextViewDelegate
 - (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - IBActions

- (IBAction)onClickBarButtons:(id)sender
{
    int tag = [sender tag];
    NSDictionary *dictAnnotation;
    
    switch (tag) {
        case 0:
            //Back button
            break;
        case 1:
            //Accept button
            dictAnnotation = [[NSDictionary alloc] initWithObjectsAndKeys:txfName.text,@"namePin",txvDescription.text,@"descPin",[NSString stringWithFormat:@"%f",coordinate.latitude],@"coordLatPin", [NSString stringWithFormat:@"%f",coordinate.longitude],@"coordLongPin",[pathImage absoluteString],@"imgPin", [NSNumber numberWithInt:appDelegate.intPinID],@"pinID", nil, nil];
            [appDelegate.mArrPins addObject:dictAnnotation];
            
            //Set data into CoreData Entity
            [self saveData];
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onClickSelectImage
{
    imgPickerPhoto = [[UIImagePickerController alloc] init];
    
    imgPickerPhoto.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [imgPickerPhoto setDelegate:(id)self];
    
    [self presentViewController:imgPickerPhoto animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [imgPinBack setImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
    
    pathImage = (NSURL *) [info valueForKey:UIImagePickerControllerReferenceURL];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Methods CoreDate
- (void)saveData {
    
    NSManagedObjectContext *context =
    [appDelegate managedObjectContext];
    
    NSManagedObject *newPin;
    newPin = [NSEntityDescription
                  insertNewObjectForEntityForName:@"Places"
                  inManagedObjectContext:context];
    double latitud, longitud;
    
    latitud = coordinate.latitude;
    longitud = coordinate.longitude;
    
    [newPin setValue: txfName.text forKey:@"namePin"];
    [newPin setValue: txvDescription.text forKey:@"descPin"];
    [newPin setValue: [NSNumber numberWithDouble:latitud] forKey:@"coordLatPin"];
    [newPin setValue:[NSNumber numberWithDouble:longitud] forKey:@"coordLongPin"];
    [newPin setValue:[pathImage absoluteString] forKey:@"imgPin"];
    [newPin setValue: [NSNumber numberWithInt:appDelegate.intPinID] forKey:@"pinID"];
    
    appDelegate.intPinID++;
    [txfName setText:@""];
    [txvDescription setText:@""];
    [imgPinBack setImage:nil];
    
    NSError *error;
    [context save:&error];
}

#pragma mark - View lifecycle

- (BOOL) shouldAutorotate
{
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Init AppDelegate
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    pathImage = [[NSURL alloc] initWithString:@""];
    
    NSLog(@"jeje: %f",coordinate.latitude);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
